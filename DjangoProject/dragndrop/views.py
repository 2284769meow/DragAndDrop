from django.http import HttpResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from dragndrop.models import AudioRecord
from os.path import splitext, exists
from django.conf import settings

# Create your views here.
def index(request):
    # calculate records with searched keywords
    audioRecordsList = []
    return render(request, 'dragndrop/index.html', {
                 'audioRecordsList': audioRecordsList,
                 'SERVER_IP': settings.SERVER_IP
                 })

@csrf_exempt
def uploadFile(request):
    file = request.FILES['file']
    # check if file is wav 
    print(file.name, splitext(file.name)[1])
    if splitext(file.name)[1] != "pcd":
        return HttpResponse("Error 403: file is not .pcd.", status=403)

    print("I have a file", file.name)

    # return OK
    return HttpResponse("File upload successully")

def showImg(request):
    actualFileName = request.GET['actualFileName']
    file_path = "dragndrop/static/dragndrop/img/" + actualFileName
    if exists(file_path):
        with open(file_path, 'rb') as fh:
            response = HttpResponse(fh.read(), content_type="image/*")
            response['Content-Disposition'] = 'inline; filename=' + actualFileName
            return response
    return HttpResponse("Error 404: file not found.", status=404)