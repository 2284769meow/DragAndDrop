from django.urls import path
from . import views

urlpatterns = [
    path('', views.index),
    path('uploadFile', views.uploadFile),
    path('showImg', views.showImg),
]
